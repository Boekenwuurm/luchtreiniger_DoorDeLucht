#include <Arduino.h>
#include <SPIFFS.h>
#include <WiFiSettings.h>
#include <ArduinoOTA.h>

#define POWER_PIN   0
#define LED_BUILTIN 19

void setup_ota() {
    ArduinoOTA.setHostname("Luchtreiniger_DoorDeLucht-");
    ArduinoOTA.setPassword("PaarsPaars");//WiFiSettings.password.c_str());
    ArduinoOTA.begin();
}

void DigitalPressCapacativeButton(int pin) {
    digitalWrite(LED_BUILTIN, HIGH);
    pinMode(pin, OUTPUT);
    digitalWrite(pin, HIGH);
    delay(500);
    //digitalWrite(pin, HIGH);
    pinMode(pin, INPUT);
    digitalWrite(LED_BUILTIN, LOW);
}

void setup() {
    Serial.begin(115200);
    //SPIFFS.format();
    SPIFFS.begin(true);  // Will format on the first run after failing to mount

    Serial.println("First handle the WiFi processes");
    // Force WPA secured WiFi for the software access point.
    // Because OTA is remote code execution (RCE) by definition, the password
    // should be kept secret. By default, WiFiSettings will become an insecure
    // WiFi access point and happily tell anyone the password. The password
    // will instead be provided on the Serial connection, which is a bit safer.
    WiFiSettings.secure = true;
    WiFiSettings.hostname = "Luchtreiniger_DoorDeLucht-";
    WiFiSettings.password = "PaarsPaars";

    // Set callbacks to start OTA when the portal is active
    WiFiSettings.onPortal = []() {
        setup_ota();
    };
    WiFiSettings.onPortalWaitLoop = []() {
        ArduinoOTA.handle();
    };

    Serial.print("Connect to the WiFi");

    // Use stored credentials to connect to your WiFi access point.
    // If no credentials are stored or if the access point is out of reach,
    // an access point will be started with a captive portal to configure WiFi.
    WiFiSettings.connect();

    Serial.print("Password: ");
    Serial.println(WiFiSettings.password);

    Serial.println("Prepair OTA");
    setup_ota();  // If you also want the OTA during regular execution

    Serial.println("Start with restarting the air cleaner");
    pinMode(LED_BUILTIN, OUTPUT);
    DigitalPressCapacativeButton(POWER_PIN);

    Serial.println("Setup done :)");
}

void loop() {
    ArduinoOTA.handle();  // If you also want the OTA during regular execution

    delay(2000);//1*60*1000);
    Serial.println("Power");
    DigitalPressCapacativeButton(POWER_PIN);
    

    // Your loop code here
}
# What
The Trotronics air purifier is a afordable air purifier. It's largest disadvantage is thet is is not starting automaticly, and it is not remote controlable. 
With a ESP32 running this code, both of these disadvantages can be resolved

# How to get started
The air purifier can be opened from the top. It is secured with some screws and a unbelievable amount of clips. I used two spurgers and some manpower to open the top. From that point, the back of the controller board is visible. There is an unpopulated header there with marked 5V and ground terminals. There are also 2 solder points for each button. 
## hardware setup
To control the buttons, solder a wire from one of the solder pads of the buttons to te microcontroller. I used a ttgo-t7-v13-mini32. The ESP can get power from the unpopuladed header.

# Future plans
* Solid startup behaviour
* other buttons
* Figure out a way to check if the machine is already on
* MQTT control



